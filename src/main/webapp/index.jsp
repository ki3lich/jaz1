<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Kalkuator rat</title>
	</head>
	<body>
		<form method="post" action="/calculate">
			<table>
				<tr>
					<td>Wnioskowana kwota kredytu:</td>
					<td><input type="number" min="0" required name="kwota"/></td>
				</tr>
				<tr>
					<td>Ilość rat:</td>
					<td><input type="number" min="0" required name="raty"/></td>
				</tr>
				<tr>
					<td>Oprocentowanie:</td>
					<td><input type="number" min="0" max="100" required name="oprocentowanie"/></td>
				</tr>
				<tr>
					<td>Opłata stała:</td>
					<td><input type="number" min="0" required name="oplata"/></td>
				</tr>
				<tr>
					<td>Rodzaj rat:</td>
					<td>
						<select name="rodzaj" required>
							<option value="malejaca">Malejąca</option>
							<option value="stala">Stała</option>
						</select>
					</td>
				</tr>
			</table>
			
			<p><input type="submit" value="Wyślij"/></p>
		</form>
	</body>
</html>
